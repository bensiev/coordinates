var Latitude = undefined;
var Longitude = undefined;
var Altitude = undefined;
var marker = undefined;

var app = {
    // Application Constructor
    initialize: function() {
		'use strict';
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
		'use strict';
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicitly call 'app.receivedEvent(...);'
    onDeviceReady: function() {
		'use strict';
       // app.receivedEvent('deviceready');
       navigator.geolocation.getCurrentPosition(app.onSuccess, app.onError, { enableHighAccuracy: true});
       var watchID = navigator.geolocation.watchPosition(app.onMapWatchSuccess, app.onError, { enableHighAccuracy: true});
    },



    onSuccess: function(position){
		'use strict';
        longitude = position.coords.longitude;
        latitude = position.coords.latitude;
		altitude = position.coords.altitude;
    },

    onError: function(error){
		'use strict';
        alert("the code is " + error.code + ". \n" + "message: " + error.message);
    },


    onMapWatchSuccess: function(position) {
		'use strict';
      var updatedLatitude = position.coords.latitude;
      var updatedLongitude = position.coords.longitude;
	  var updatedAltitude = position.coords.altitude;

      if (updatedLatitude != Latitude && updatedLongitude != Longitude) {

          Latitude = updatedLatitude;
          Longitude = updatedLongitude;
		  Altitude = updatedAltitude;
		  
		  //Update html to display coordinates
		  document.getElementById("lat").innerHTML = "Latitude = " + Latitude;
		  document.getElementById("long").innerHTML = "Longitude = " + Longitude;
		  document.getElementById("alt").innerHTML = "Altitude = " + Altitude;
      }
    },
};

function onload() {
	'use strict';
	document.getElementById('recordbtn').addEventListener('touchstart', record);
	document.getElementById('resetbtn').addEventListener('touchstart', reset);
}

function record() {
	'use strict';
	document.getElementById('list').innerHTML += 
		"Latitude = " + Latitude + "<br>" +
		"Longitude = " + Longitude + "<br>" +
		"Altitude = " + Altitude + "<br> <br>";
}

function reset() {
	'use strict';
	document.getElementById('list').textContent = '';
}


app.initialize();

document.addEventListener('touchend');